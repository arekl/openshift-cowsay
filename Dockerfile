FROM debian
RUN apt-get update
RUN apt-get install -y fortune cowsay
COPY entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]

